[![codecov](https://codecov.io/gl/davi.abreu.w/finitomata/branch/master/graph/badge.svg)](https://codecov.io/gl/davi.abreu.w/finitomata)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
# finitomata

Finitomata é uma tentativa de criar máquinas de estado de controle usando paradigma declarativo em Python.

Deseja-se alcançar de forma automatizada:
* *Diagrama -> Código*
* *Código -> Diagrama*
* *Exportação: JSON, XML, etc*

A notação atualmente usada foi gerada através da ferramenta draw.io e exportada como PNG e XML(somente recorte de interesse - sem legenda).

* finitomata.py - traz a máquina executora.
* main.py - traz a descrição fidedigna da máquina escrita por um ser humano olhando o mapa 'Open_Keep.png'
* my.xml - traz uma exportação do recorte de interesse do diagrama da máquina
* xmldecoder.py - é o parser do xml gerado pelo draw.io para fazer o *Diagrama -> Código*

===

# Setup virtual environment<br>
$ sudo -H pip3 install virtualenv<br>
$ python3 -m virtualenv --python=python3 venv

# Active virtual environment<br>
$ source venv/bin/activate

# Deactive virtual environment<br>
$ deactivate

# Setup Tests<br>
$ pip install -r test/requirements.txt

# Run Hacking<br>
$ flake8 --jobs='auto' --output-file=output/linter.txt finitomata/

# Run all tests:<br>
$ pytest

# Run pytest with code coverage:<br>
$ pytest -v --cov=finitomata

# Generating html artifacts:<br>
* For unit/functional/integration tests: --html=output/tests.html
* For coverage: --cov-report=html:output/coverage

# Run all tests options:<br>
$ pytest -v --cov=finitomata --html=output/tests.html --cov-report=html:output/coverage
