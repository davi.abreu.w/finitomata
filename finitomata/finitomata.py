import asyncio
from enum import Enum
from pprint import pprint


class NotAnEnum(AttributeError):

    def __init__(self, label):
        self.label = label

    def __str__(self):
        return "Event label '{}' must be an 'Enum'".format(self.label)


class NotAnAction(AttributeError):

    def __init__(self, action):
        self.action = action

    def __str__(self):
        return "Transition '{}' is not an Action".format(self.action)


class NotAListener(AttributeError):

    def __init__(self, listener):
        self.listener = listener

    def __str__(self):
        return "'{}' is not a listener".format(self.listener)


class NotAnAccumulator(AttributeError):

    def __init__(self, symbol):
        self.symbol = symbol

    def __str__(self):
        return "'{}' is not an accumulator".format(self.symbol)


class NotFoundExtradata(AttributeError):

    def __init__(self, label, machine):
        self.label = label
        self.machine = machine

    def __str__(self):
        return "Requested extra data '{}' is not " \
            "registered in the machine '{}'" \
            .format(self.label, self.machine)


class NotInitialized(RuntimeError):

    def __init__(self, automata_id):
        self.automata_id = automata_id

    def __str__(self):
        return "Automata '{}' have not an initial state" \
            .format(self.automata_id)


class DeadState(RuntimeError):

    def __init__(self, state):
        self.state = state

    def __str__(self):
        return "Dead state '{}'".format(self.state)


class InvalidTransition(RuntimeError):

    def __init__(self, state, event):
        self.state = state
        self.event = event

    def __str__(self):
        return "Transition '{}[{}]' is not registered" \
            .format(self.state, self.event)


class Finitypes(Enum):
    """Default machine Types"""

    ANY_STATE = 0
    LISTENER = 1


class Listener(object):
    """Default Listener interface"""

    def listen(self, automata_id, event, **params):
        pass

    def __repr__(self):
        return 'Listener {}'.format(type(self).__name__)


class Action(object):
    """Abstraction of action to the machine"""

    def act(self, machine):
        pass

    def __repr__(self):
        return 'Action {} {}'.format(
            type(self).__name__,
            self.__dict__
        )


class ChangeState(Action):
    """Go to state

        1. Cancel schedule_slot if a call_later is scheduled
        2. Change state
        3. Execute state action if provided
    """

    def __init__(self, state):
        self.state = state

    def act(self, machine):
        if machine.schedule_slot is not None:
            machine.schedule_slot.cancel()
            machine.schedule_slot = None
        machine.change_state(self.state)
        action = machine.state_action_map.get(machine.state)
        if action is not None:
            action.act(machine)


class SendEvent(Action):
    """Send provided state to Listener.state

            * If extra_data_list is provided,
                each element requested in machine extra_data
                will be sent as kwargs
        Obs: the machine need to have a Finitypes.Listener
            registered in extra_data
    """

    def __init__(self, event, *extra_data_list):
        self.event = event
        self.extra_data_list = extra_data_list

    def act(self, machine):
        params = {}
        for label in self.extra_data_list:
            if not isinstance(label, Enum):
                raise NotAnEnum(label)
            data = machine.get(label)
            if label is None:
                raise NotFoundExtradata(label, machine)
            params[label.name] = data
        listener = machine.get(Finitypes.LISTENER)
        if not isinstance(listener, Listener):
            raise NotAListener(listener)
        listener.listen(machine.automata_id, self.event, **params)


class SendEventAndChangeState(SendEvent, ChangeState):
    """Send event and go to state

        1. Execute SendEvent
        2. Execute ChangeState
    """

    def __init__(self, event, state, *extra_data_list):
        SendEvent.__init__(self, event, *extra_data_list)
        ChangeState.__init__(self, state)

    def act(self, machine):
        SendEvent.act(self, machine)
        ChangeState.act(self, machine)


class TimeoutChangeState(ChangeState):
    """Run Status Change after a timeout

    'time' is a class with a slot named 'value' representing an integer

    1. Add a call_later in machine.schedule_slot
    2. If call_later trigger execute ChangeState
    """

    def __init__(self, time, state):
        ChangeState.__init__(self, state)
        self.time = time

    def delay_act(self, machine):
        ChangeState.act(self, machine)

    def act(self, machine):
        machine.schedule(self.time.value, self.delay_act)


class IncrementAccumulator(Action):
    """Increment Accumulator

    Increment previous registered Accumulator
    in the machine with the given amount.

    Obs: the machine need to have an Accumulator
        with value int registered in extra_data
    """

    def __init__(self, symbol, amount=1):
        self.symbol = symbol
        self.amount = amount

    def act(self, machine):
        accumulator = machine.get(self.symbol)
        if not isinstance(accumulator, int):
            raise NotAnAccumulator(self.symbol)
        machine.set(self.symbol, accumulator + self.amount)


class IncrementAccumulators(Action):
    """Increment more than one Accumulator

    Increment previous registered list of Accumulators
        in the machine with the given amount
    Amount is used equaly to all acumulators
    Obs: the machine need to have all the Accumulators with value
        int registered in extra_data
    """
    def __init__(self, symbol_list, amount=1):
        symbol_dict = {}
        for symbol in symbol_list:
            symbol_dict[symbol] = IncrementAccumulator(symbol, amount).act
        self.symbol_dict = symbol_dict

    def act(self, machine):
        for symbol in self.symbol_dict:
            self.symbol_dict[symbol](machine)


class DecrementAccumulator(Action):
    """Increment Accumulator

    Decrement previous registered Accumulator in the machine
        with the given amount
    Obs: the machine need to have an Accumulator
        with value int registered in extra_data
    """

    def __init__(self, symbol, amount=1):
        self.symbol = symbol
        self.amount = amount

    def act(self, machine):
        accumulator = machine.get(self.symbol)
        if not isinstance(accumulator, int):
            raise NotAnAccumulator(self.symbol)
        machine.set(self.symbol, accumulator - self.amount)


class ResetAccumulator(Action):
    """Set to 0 one Accumulator

    Set previous registered Accumulator in the machine
        with to 0
    Obs: the machine need to have an Accumulator
        with value int registered in extra_data
    """

    def __init__(self, symbol):
        self.symbol = symbol

    def act(self, machine):
        accumulator = machine.get(self.symbol)
        if not isinstance(accumulator, int):
            raise NotAnAccumulator(self.symbol)
        machine.set(self.symbol, 0)


class ResetAccumulators(Action):
    """Set to 0 more than one Accumulator

    Set previous registered list of Accumulators
        in the machine with to 0
    Obs: the machine need to have all the Accumulators with value
        int registered in extra_data
    """

    def __init__(self, symbol_list):
        symbol_dict = {}
        for symbol in symbol_list:
            symbol_dict[symbol] = ResetAccumulator(symbol).act
        self.symbol_dict = symbol_dict

    def act(self, machine):
        for symbol in self.symbol_dict:
            self.symbol_dict[symbol](machine)


class SendAndResetAccumulatorsAndChangeState(
        SendEvent,
        ResetAccumulators,
        ChangeState
):
    """Send, Reset more than one acummulator before Change State

    Send and reset accumulators before change State
    This is specific for integers extra_data
        even if not used as an accumulator
    """

    def __init__(self, event, state, *extra_data_list):
        SendEvent.__init__(self, event, *extra_data_list)
        ResetAccumulators.__init__(self, extra_data_list)
        ChangeState.__init__(self, state)

    def act(self, machine):
        SendEvent.act(self, machine)
        ResetAccumulators.act(self, machine)
        ChangeState.act(self, machine)


class PrintAccumulator(Action):
    def __init__(self, symbol):
        self.symbol = symbol

    def act(self, machine):
        SendEventAndChangeState.act(machine)


class FiniteAutomata(object):

    """Machine that runs the Automata

    state_action_map: has State as key and an Action class as value
    Its used to do Action when reaching the state

    Transition Map is a composite map describing the transitions
    Primary Key is Source State
        Second Key is the transition event
            Final Value: is an Action to do when occur the transition
    """

    def __init__(
        self,
        automata_id,
        initial_state,
        state_action_map,
        transition_map,
        extra_data=None
    ):
        self.automata_id = automata_id
        self.state = None
        self.state_action_map = state_action_map
        self.transition_map = transition_map
        if Finitypes.ANY_STATE in transition_map:
            any_map = self.transition_map.pop(Finitypes.ANY_STATE)
            for state in self.transition_map:
                for event in any_map:
                    if event not in self.transition_map[state]:
                        self.transition_map[state][event] = any_map[event]
        self.schedule_slot = None
        if extra_data is None:
            extra_data = {}
        self.extra_data = extra_data
        if initial_state is None:
            raise NotInitialized(automata_id)
        ChangeState(initial_state).act(self)

    def __str__(self):
        return "{}".format(self.automata_id)

    def get(self, symbol):
        return self.extra_data[symbol]

    def set(self, symbol, value):
        self.extra_data[symbol] = value

    def on_change(self, state):
        """Hook when changing state."""
        pass

    def change_state(self, state):
        self.state = state
        self.on_change(state)

    def schedule(self, time, func, *params):
        loop = asyncio.get_event_loop()
        self.schedule_slot = loop.call_later(time, func, self, *params)

    def kill(self):
        if self.schedule_slot is not None:
            self.schedule_slot.cancel()

    def transition(self, event):
        transitions = self.transition_map.get(self.state)
        if transitions is None:
            raise DeadState(self.state)
        action = transitions.get(event)
        if not isinstance(action, Action):
            if action is None:
                raise InvalidTransition(self.state, event)
            raise NotAnAction(action)
        action.act(self)
        return self.state

    def print(self, level=0):
        if level > 0:
            print("===     State      ===")
            pprint(self.state)
        if level > 1:
            print("===   Reach map    ===")
            pprint(self.state_action_map)
        if level > 2:
            print("=== Transition map ===")
            pprint(self.transition_map)

    def __del__(self):
        print("DELETING")
        if self.schedule_slot is not None:
            self.schedule_slot.cancel()
