# from pprint import pformat
from bs4 import BeautifulSoup
from enum import Enum

file = open("my.xml")
soup = BeautifulSoup(file, "xml")
# print(soup.__dict__)


class Types(Enum):
    EDGE = 1
    GROUP = 2
    SIMPLE_STATE = 3
    MESSAGE_STATE = 4
    INIT_AGGREGATION = 5
    TIMEOUT_AGGREGATION = 6

# class Element():

#     def __init__(self, xml_item):
#         self.id = xml_item.get("id")
#         value = item.get("value")
#         if value is not None:
#             value = self.sanitize(value)
#         self.value = value
#         style = item.get("style")
#         if style is not None:
#             if style == "group":
#                 style = {"group" : True}
#             else:
#                 print(style)
#                 style = self.stylize(style)
#                 style["group"] = False
#         self.style = style

#     def sanitize(self, phrase):
#         remove_elements = ["<br>", "</span>", "<span>"]
#         for e in remove_elements:
#             phrase = phrase.replace(e, "")
#         return phrase

#     def stylize(self, phrase):
#         phrase_dict = {}
#         phrase = phrase.split(";")[:-1]
#         for pair in phrase:
#             pair = pair.split("=")
#             phrase_dict[pair[0]] = pair[1]
#         return phrase_dict

#     def __str__(self):
#         return pformat(self.__dict__, indent=4)


def get_type(item_id, edge, style):
    if edge:
        return Types.EDGE
    elif "group" == style:
        return Types.GROUP
    elif style is not None:
        if "shape=mxgraph.bpmn.shape" in style:
            if "symbol=general" in style:
                return Types.SIMPLE_STATE
            if "symbol=message" in style:
                return Types.MESSAGE_STATE
            if "symbol=escalation" in style:
                return Types.INIT_AGGREGATION
            if "symbol=timer" in style:
                return Types.TIMEOUT_AGGREGATION
        elif "text" in style:
            raise AttributeError("Some label is disconected from source")
    raise AttributeError("mxCell id '{}' is not mapped".format(item_id))

interest_elements = soup.find_all("mxCell")
print("Elements of interest", len(interest_elements))
for item in interest_elements:
    item_id = item.get("id")
    edge = item.get("edge")
    style = item.get("style")
    if style is None:
        print("Skipping id '{}'".format(item_id))
        continue  # Not mapped nor interest element
    print(get_type(item_id, edge, style))
    # element = Element(item)
    # print(element)
