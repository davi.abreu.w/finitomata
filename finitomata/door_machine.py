from finitomata.finitomata import ChangeState
from finitomata.finitomata import FiniteAutomata
from finitomata.finitomata import Finitypes
from finitomata.finitomata import IncrementAccumulator
from finitomata.finitomata import Listener
from finitomata.finitomata import ResetAccumulators
from finitomata.finitomata import SendEvent
from finitomata.finitomata import SendEventAndChangeState
from finitomata.finitomata import TimeoutChangeState
from finitomata import module_configs

from enum import Enum


# Configurables
NOT_OPENED_TIMEOUT = module_configs.NOT_OPENED_TIMEOUT
NOT_CLOSED_TIMEOUT = module_configs.NOT_CLOSED_TIMEOUT
# //


class Inputs(Enum):
    COMMAND_OPEN = 1
    COMMAND_CLOSE = 2
    COMMAND_KEEP = 3
    SENSOR_DOOR_OPENED = 4
    SENSOR_DOOR_CLOSED = 5
    ACCESS_GRANTED = 6
    ACCESS_DONE = 7


class States(Enum):
    INIT = 1
    OPENING = 2
    CLOSED = 3
    OPENED = 4
    RETAINED = 5
    RETAINING = 6
    BREAK_IN = 7
    DID_NOT_OPEN = 8
    DID_NOT_CLOSE = 9
    RESOLVE_RETENTION = 10
    COMPARE_ACCUMULATOR = 11
    NOBODY_ACCESSED = 12
    RIDE_ACCESSED = 13


class Accumulators(Enum):
    ACCESS = 1
    MAX_ACCESS = 2


ACCUMULATORS = [Accumulators.ACCESS, Accumulators.MAX_ACCESS]


class CompareAccessAccumulators(SendEventAndChangeState):

    def __init__(self):
        self.nobody_accessed = SendEvent(States.NOBODY_ACCESSED)
        self.ride_accessed = SendEvent(States.RIDE_ACCESSED)
        SendEventAndChangeState.__init__(
            self, States.COMPARE_ACCUMULATOR,
            States.CLOSED
        )

    def act(self, machine):
        access = machine.get(Accumulators.ACCESS)
        max_access = machine.get(Accumulators.MAX_ACCESS)
        if max_access != 0 and access == 0:
            self.nobody_accessed.act(machine)
        elif max_access != 0 and access > max_access:
            self.ride_accessed.act(machine)
        SendEventAndChangeState.act(self, machine)


STATE_ACTION_MAP = {
    States.CLOSED:
        ResetAccumulators(
            ACCUMULATORS
        ),
    States.RETAINING:
        TimeoutChangeState(
            NOT_OPENED_TIMEOUT,
            States.DID_NOT_OPEN
        ),
    States.OPENING:
        TimeoutChangeState(
            NOT_OPENED_TIMEOUT,
            States.DID_NOT_OPEN
        ),
    States.OPENED:
        TimeoutChangeState(
            NOT_CLOSED_TIMEOUT,
            States.DID_NOT_CLOSE
        ),
    States.BREAK_IN:
        SendEventAndChangeState(
            States.BREAK_IN,
            States.OPENED
        ),
    States.DID_NOT_OPEN:
        SendEventAndChangeState(
            States.DID_NOT_OPEN,
            States.COMPARE_ACCUMULATOR
        ),
    States.DID_NOT_CLOSE:
        SendEventAndChangeState(
            States.DID_NOT_CLOSE,
            States.OPENED
        ),
    States.RESOLVE_RETENTION:
        SendEventAndChangeState(
            States.RESOLVE_RETENTION,
            States.CLOSED
        ),
    States.COMPARE_ACCUMULATOR:
        CompareAccessAccumulators()
}

TRANSITION_MAP = {
    States.INIT:
    {
        Inputs.SENSOR_DOOR_OPENED:
            ChangeState(States.RETAINED),
        Inputs.SENSOR_DOOR_CLOSED:
            ChangeState(States.CLOSED),
    },
    States.OPENING:
    {
        Inputs.ACCESS_GRANTED:
            IncrementAccumulator(Accumulators.MAX_ACCESS),
        Inputs.ACCESS_DONE:
            IncrementAccumulator(Accumulators.ACCESS),
        Inputs.SENSOR_DOOR_OPENED:
            ChangeState(States.OPENED)
    },
    States.CLOSED:
    {
        Inputs.ACCESS_GRANTED:
            IncrementAccumulator(Accumulators.MAX_ACCESS),
        Inputs.ACCESS_DONE:
            IncrementAccumulator(Accumulators.ACCESS),
        Inputs.SENSOR_DOOR_OPENED:
            ChangeState(States.BREAK_IN),
        Inputs.COMMAND_OPEN:
            ChangeState(States.OPENING),
        Inputs.COMMAND_KEEP:
            ChangeState(States.RETAINING)
    },
    States.OPENED:
    {
        Inputs.ACCESS_GRANTED:
            IncrementAccumulator(Accumulators.MAX_ACCESS),
        Inputs.ACCESS_DONE:
            IncrementAccumulator(Accumulators.ACCESS),
        Inputs.SENSOR_DOOR_CLOSED:
            ChangeState(States.COMPARE_ACCUMULATOR)
    },
    States.RETAINED:
    {
        Inputs.COMMAND_CLOSE:
            ChangeState(States.OPENED),
        Inputs.SENSOR_DOOR_CLOSED:
            ChangeState(States.RESOLVE_RETENTION)
    },
    States.RETAINING:
    {
        Inputs.SENSOR_DOOR_OPENED:
            ChangeState(States.RETAINED)
    }
}


class DoorMachine(Listener):

    def __init__(self, automata_id, init_state=States.INIT):
        self.running = True
        automata_id = automata_id
        extra_data = {
            Finitypes.LISTENER: self,
            Accumulators.ACCESS: 0,
            Accumulators.MAX_ACCESS: 0
        }
        self.automata = FiniteAutomata(
            automata_id,
            States.INIT,
            STATE_ACTION_MAP,
            TRANSITION_MAP,
            extra_data
        )

    def state(self):
        return States(self.automata.state.value)

    def listen(self, automata_id, event, **params):
        pass

    def tell(self, event):
        self.automata.transition(event)

    def kill(self):
        self.automata.kill()

    def print(self, level=1):
        self.automata.print(level)
