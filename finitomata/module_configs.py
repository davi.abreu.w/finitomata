class Configurable(object):

    def __init__(self, value):
        self.value = value

NOT_OPENED_TIMEOUT = Configurable(13)
NOT_CLOSED_TIMEOUT = Configurable(60)
