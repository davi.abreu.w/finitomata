from .finitomata_test_infra import Automata
from finitomata.door_machine import Inputs, States
from finitomata.door_machine import DoorMachine


################
# Start closed #
################
@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.SENSOR_DOOR_OPENED
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.BREAK_IN,
        States.OPENED
    ],
    expected_events=[
        States.BREAK_IN
    ]
)
def test_breakin():
    """Breakin Test"""
    pass


@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.COMMAND_KEEP,
        Inputs.SENSOR_DOOR_OPENED,
        Inputs.COMMAND_CLOSE,
        Inputs.SENSOR_DOOR_CLOSED
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.RETAINING,
        States.RETAINED,
        States.OPENED,
        States.COMPARE_ACCUMULATOR,
        States.CLOSED
    ],
    expected_events=[
        States.COMPARE_ACCUMULATOR
    ]
)
def test_closed_retain_then_close_the_door():
    """Start Closed, retain the door then close the door"""
    pass


@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.COMMAND_OPEN,
        Inputs.SENSOR_DOOR_OPENED,
        Inputs.SENSOR_DOOR_CLOSED
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.OPENING,
        States.OPENED,
        States.COMPARE_ACCUMULATOR,
        States.CLOSED
    ],
    expected_events=[
        States.COMPARE_ACCUMULATOR
    ]
)
def test_closed_open_the_door():
    """Start Closed, open the door then close the door"""
    pass


@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.ACCESS_GRANTED,
        Inputs.COMMAND_OPEN,
        Inputs.SENSOR_DOOR_OPENED,
        Inputs.SENSOR_DOOR_CLOSED
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.OPENING,
        States.OPENED,
        States.COMPARE_ACCUMULATOR,
        States.CLOSED
    ],
    expected_events=[
        States.NOBODY_ACCESSED,
        States.COMPARE_ACCUMULATOR
    ]
)
def test_nobody_accessed():
    """Generate an RF granted access, open then close the door without any CAR access done"""
    pass


@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.ACCESS_GRANTED,
        Inputs.COMMAND_OPEN,
        Inputs.SENSOR_DOOR_OPENED,
        Inputs.ACCESS_DONE,
        Inputs.ACCESS_DONE,
        Inputs.SENSOR_DOOR_CLOSED
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.OPENING,
        States.OPENED,
        States.COMPARE_ACCUMULATOR,
        States.CLOSED
    ],
    expected_events=[
        States.RIDE_ACCESSED,
        States.COMPARE_ACCUMULATOR
    ]
)
def test_simple_ride_accessed():
    """Generate an RF granted access, open then close the door with more than one access done"""
    pass


################
# Start opened #
################
@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_OPENED,
        Inputs.SENSOR_DOOR_CLOSED
    ],
    expected_states=[
        States.INIT,
        States.RETAINED,
        States.RESOLVE_RETENTION,
        States.CLOSED
    ],
    expected_events=[
        States.RESOLVE_RETENTION
    ]
)
def test_retained_resolve_retention():
    """Start retained then resolve retention"""
    pass


@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_OPENED,
        Inputs.COMMAND_CLOSE,
        Inputs.SENSOR_DOOR_CLOSED
    ],
    expected_states=[
        States.INIT,
        States.RETAINED,
        States.OPENED,
        States.COMPARE_ACCUMULATOR,
        States.CLOSED
    ],
    expected_events=[
        States.COMPARE_ACCUMULATOR
    ]
)
def test_retained_close_the_door():
    """Start retained then close the door"""
    pass
