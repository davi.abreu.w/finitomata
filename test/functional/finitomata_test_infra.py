import asyncio
from finitomata.finitomata import InvalidTransition
from finitomata.finitomata import FiniteAutomata
from finitomata.door_machine import DoorMachine
from enum import Enum


class Timeout():
    """Used to sleep for a time to catch a timeout"""
    def __init__(self, seconds):
        self.seconds = seconds

    def __str__(self):
        return "Timeout <{} seconds>".format(self.seconds)

    @asyncio.coroutine
    def sleep(self):
        yield from asyncio.sleep(self.seconds)


@asyncio.coroutine
def exec_flow(teller, inputs_queue, states_queue, events_queue):
    def on_change(state):
        states_queue.append(state)

    def add_queue(automata_id, event, **kwargs):
        print("===     Notify     ===")
        print(event)
        events_queue.append(event)
    teller.automata.on_change = on_change
    teller.listen = add_queue
    states_queue.append(teller.state())
    teller.print(1)
    for event in inputs_queue:
        print("===   Transition   ===")
        print(event)
        if isinstance(event, Timeout):
            yield from event.sleep()
            continue
        try:
            teller.tell(event)
        except InvalidTransition as e:
            states_queue.append(InvalidTransition)
        teller.print(1)
    teller.kill()


def report_flow(
        teller, inputs_queue, states_queue, expected_states_queue,
        events_queue, expected_events_queue
):
    print()
    print("+++     FINAL      +++")
    teller.print(1)
    print("===  Inputs Queue  ===")
    print(inputs_queue)
    print("===  States Queue  ===")
    print(states_queue)
    print("===Expected States ===")
    print(expected_states_queue)
    assert expected_states_queue == states_queue
    print("===  Events Queue  ===")
    print(events_queue)
    print("===Expected Events ===")
    print(expected_events_queue)
    assert expected_events_queue == events_queue


def run(teller, inputs_queue, expected_states_queue, expected_events_queue):
    states_queue = []
    events_queue = []
    loop = asyncio.get_event_loop()
    loop.run_until_complete(exec_flow(teller, inputs_queue, states_queue, events_queue))
    report_flow(teller, inputs_queue, states_queue, expected_states_queue,
                events_queue, expected_events_queue)


class Automata(object):

    def __init__(self, automata, inputs=[], expected_states=[], expected_events=[]):
        self.automata = automata
        self.inputs = inputs
        self.expected_states = expected_states
        self.expected_events = expected_events

    def __call__(self, func):
        def to_run(*args, **kwargs):
            return run(self.automata, self.inputs, self.expected_states, self.expected_events)
        return to_run
