from .finitomata_test_infra import Automata
from .finitomata_test_infra import Timeout
from finitomata import module_configs
from finitomata.finitomata import InvalidTransition
from finitomata.door_machine import Inputs, States
from finitomata.door_machine import DoorMachine


NOT_OPENED_TIMEOUT = 1
NOT_CLOSED_TIMEOUT = 2

module_configs.NOT_OPENED_TIMEOUT.value = NOT_OPENED_TIMEOUT
module_configs.NOT_CLOSED_TIMEOUT.value = NOT_CLOSED_TIMEOUT


###############
#Start closed #
###############
@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.COMMAND_OPEN,
        Timeout(NOT_OPENED_TIMEOUT + 1)
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.OPENING,
        States.DID_NOT_OPEN,
        States.COMPARE_ACCUMULATOR,
        States.CLOSED
    ],
    expected_events=[
        States.DID_NOT_OPEN,
        States.COMPARE_ACCUMULATOR
    ]
)
def test_closed_open_timeout():
    """Start Closed, send open and wait timeout"""
    pass


@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.COMMAND_KEEP,
        Timeout(NOT_OPENED_TIMEOUT + 1)
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.RETAINING,
        States.DID_NOT_OPEN,
        States.COMPARE_ACCUMULATOR,
        States.CLOSED
    ],
    expected_events=[
        States.DID_NOT_OPEN,
        States.COMPARE_ACCUMULATOR
    ]
)
def test_closed_keep_timeout():
    """Start Closed, send keep and wait timeout"""
    pass


@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.COMMAND_OPEN,
        Inputs.SENSOR_DOOR_OPENED,
        Timeout(NOT_CLOSED_TIMEOUT + 1)
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.OPENING,
        States.OPENED,
        States.DID_NOT_CLOSE,
        States.OPENED
    ],
    expected_events=[
        States.DID_NOT_CLOSE
    ]
)
def test_closed_open_not_closed_timeout():
    """Start Closed, send open then open and wait timeout"""
    pass


@Automata(
    DoorMachine("DoorTest"),
    inputs=[
        Inputs.SENSOR_DOOR_CLOSED,
        Inputs.COMMAND_KEEP,
        Inputs.SENSOR_DOOR_OPENED,
        Inputs.COMMAND_CLOSE,
        Timeout(NOT_CLOSED_TIMEOUT + 1)
    ],
    expected_states=[
        States.INIT,
        States.CLOSED,
        States.RETAINING,
        States.RETAINED,
        States.OPENED,
        States.DID_NOT_CLOSE,
        States.OPENED
    ],
    expected_events=[
        States.DID_NOT_CLOSE
    ]
)
def test_closed_keep_close_not_closed_timeout():
    """Start Closed, send keep then open, close and wait timeout"""
    pass
