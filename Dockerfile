FROM python:3.4-alpine

WORKDIR /usr/src/app

COPY test/requirements.txt ./
RUN pip install -r requirements.txt

COPY . .
